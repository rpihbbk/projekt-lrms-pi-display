import lcddriver
from time import *

import requests
import json

print("init")

with open("LRMS.conf") as f:
	array = []
	for line in f:
		array.append(line.replace('\n',''))
	f.close

for info in array:
	print(info)

request = "http://" + str(array[1]) + ":8080/item_in_use/" + str(array[0])

print(request)

lcd = lcddriver.lcd()

while 1:
	r = requests.get(request)
	jsonRequest = r.text
	print(jsonRequest)
	items = json.loads(jsonRequest)
	lcd.lcd_clear()

	if items["type"] == 0:
		lcd.lcd_display_string("Laptopwagen",1)

	lcd.lcd_display_string("Raum: " + items["name"],2)

	if items["reserved"] == True:
		lcd.lcd_display_string("Reseviert",4)
	else:
		lcd.lcd_display_string("Nicht reserviert",4)

	sleep(float(array[2]))
